package com.shieldsquare.vertx.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.shieldsquare.vertx.model.VertxBean;
import com.shieldsquare.vertx.utils.CSV_Generator;

import redis.clients.jedis.Jedis;

public class CFMData_Check {

	Logger log;

	public CFMData_Check(Logger log2) {
		// TODO Auto-generated constructor stub
		log = log2;
	}

	public void monitor_ModeData(String rules_value, int sid,VertxBean ver_bean)
			throws ParseException {

		System.out
				.println("*********Monitor Mode Data Checking in CFM***********");

		log.info("");
		log.info("****Inside Monitor mode Data Check in CFM****");

		String isbot = "isBot";
		String recievedTime = "recvdTime";
		String response_code = "ssresp";
		String rules = rules_value;
		String ip_add = "11.122.11.11";

		Jedis jedis = new Jedis("104.239.157.67");
		jedis.auth("redis@123@Azure");
		System.out.println("Connected CFM Redis for apilist");
		log.info("Connection done With CFM Redis");
		List<String> li = jedis.lrange("apilist", 0, -1);

		System.out.println("Total request Present in apilist"
				+ jedis.llen("apilist"));
		log.info("Total Request Present in apilist" + jedis.llen("apilist"));

		System.out
				.println("____________________________________________________");

		for (int i = 0; i <= li.size() - 1; i++) {
			String lis = li.get(i);
			// System.out.println(lis);
			JSONParser m = new JSONParser();
			JSONObject jsonObject = (JSONObject) m.parse(lis.toString());

			// if(jsonObject.get("_zpsbd8")!="6"&&jsonObject.get("_zpsbd6")==ip_add){
			//if (jsonObject.get("_zpsbd8") != "6") {

				log.info("Checking Parameter which is added by Vertx in packet");
				if (lis.contains(isbot) && lis.contains(recievedTime)
						&& lis.contains(response_code) && lis.contains(rules)) {

					log.info("packet Contain isBot, reciveTime, response code, rules");

					String resCode = jsonObject.get("ssresp").toString();
					String isbot1 = jsonObject.get("isBot").toString();
					String recievedTime1 = jsonObject.get("recvdTime").toString();
					String rules5_1 = jsonObject.get(rules).toString();
					
					
					log.info("Contain Parameter value in Packet is :-");
					log.info("isBot -->> " + isbot1);
					log.info("recievedTime  -->> " + recievedTime1);
					log.info("response Code  -->> " + resCode);
					log.info("rules_failed(" + rules + ")  -->> " + rules5_1);

					log.info("Checking Vertx added parameter value is correct");
					MRM_Set_Values rv = new MRM_Set_Values(log);
					String get_rules_value = rv.rules_Value_MRM(sid, rules, ver_bean);
					ver_bean.setCust_config_rules(get_rules_value);
					if (isbot1.equalsIgnoreCase("true")
							&& rules5_1.equalsIgnoreCase("true")) {
						log.info("'isBot '= 'true' and 'rules_failed = 'true' Present in Packet");
						ver_bean.setIsBot_Param(isbot1);
						ver_bean.setRulsfail_Param(rules +" is "+rules5_1);
						if (resCode.equalsIgnoreCase("0")) {

							log.info("Packet have all parameter with correct value");
							ver_bean.setSsresp_Param(resCode);
							ver_bean.setTest_status("((*Test Pass*))");
							ver_bean.setTest_Pass_Desc("All required Parameter with expected value Present In request");
							System.out.println("yaya yaha par  2");
							log.info("***--########((((Test Pass))))########--***");
							// log.info("isbot=true, "+rules5_1+" = true  and response code = "
							// +get_rules_value);
						} else {
							log.info("***--((((Test Fail))))--***");
							ver_bean.setTest_status("((*Fail*))");
							ver_bean.setTest_Fail_Desc("Response value is not '0' it is "+rules5_1);
							System.out.println("yaya yaha par  3");
						}
					} else {
						log.info("***--((((Test Fail))))--***");
						ver_bean.setTest_status("((*Fail*))");
						ver_bean.setTest_Fail_Desc("isBot or rules failed value is not true in packet for Active mode");
						System.out.println("yaya yaha par  4");
					}

				} else {
					System.out
							.println("Vertx added Parameter not present in Request");
					log.info("Proper parameter not present in Monitor mode request like isBot true,responcetime etc..");
					ver_bean.setTest_Fail_Desc("Required Parameter Not Present in Request");
				}
			/*} else {
				if (jsonObject.get("_zpsbd6") == "6") {
					log.info("Packet contain CallType 6");
					calType6(lis);
				} else {
					log.info("Sent IP not matched in Packet");
				}
			}*/
		}
		log.info("deleting apilist");
		System.out.println("Deleting apilist in Monitor Mode");
		jedis.del("apilist");
		log.info("apilist is deleted");
		System.out.println("CFM checking DONE");
		log.info("Data Checking done");
		log.info("");
	}

	// For Active Mode

	public void active_ModeData(String rules_value, int sid, VertxBean ver_bean)
			throws ParseException, IOException {
		log.info("*Inside 'Active mode Data Check in CFM' Method*");
		String isbot = "isBot";
		String recievedTime = "recvdTime";
		String response_code = "ssresp";
		String rules = rules_value;
		System.out.println("value sissssssssssssssssssssssssssssssssssss--  "+rules);
		Jedis jedis = new Jedis("104.239.157.67");
		jedis.auth("redis@123@Azure");

		VertxBean vb = new VertxBean();
		List<String> li = jedis.lrange("apilist", 0, -1);
		for (int i = 0; i <= li.size() - 1; i++) {
			String lis = li.get(i);
			
				
				if (lis.contains(isbot) && lis.contains(recievedTime)
						&& lis.contains(response_code) && lis.contains(rules)) {
					System.out.println("value sissasasasTTTTTTTTTTTTsssssssss--  "+rules);
					System.out.println("yaya isbot mai");
					log.info("Packet Contain 'isBot', 'reciveTime', 'response code', 'rules_failed(like r1,r2,r4,r5...etc)'");

					JSONParser m = new JSONParser();
					JSONObject jsonObject = (JSONObject) m
							.parse(lis.toString());
					String resCode = jsonObject.get("ssresp").toString();
					String isbot1 = jsonObject.get("isBot").toString();
					String recievedTime1 = jsonObject.get("recvdTime")
							.toString();
					String rules5_1 = jsonObject.get(rules).toString();

					log.info("Contain Parameter value in Packet is :-");
					log.info("isBot -->> " + isbot1);
					log.info("recievedTime  -->> " + recievedTime1);
					log.info("response Code  -->> " + resCode);
					log.info("rules_failed(" + rules + ")  -->> " + rules5_1);

					log.info("");
					log.info("*Calling 'SID-hash-status' method to get value from 'H:sid:<sid>'*");

					MRM_Set_Values rv = new MRM_Set_Values(log);
					String get_rules_value = rv.rules_Value_MRM(sid, rules,ver_bean);
					
					ver_bean.setCust_config_rules(get_rules_value);
					log.info("Value for '" + rules
							+ "' is Set in H:sid:<sid> is  " + get_rules_value);

					log.info("Checking in Packet all added parameter are with its correct value or not");
					
					
					
					if (isbot1.equalsIgnoreCase("true")
							&& rules5_1.equalsIgnoreCase("true")) {
						log.info("'isBot '= 'true' and 'rules_failed = 'true' Present in Packet");
						ver_bean.setIsBot_Param(isbot1);
						ver_bean.setRulsfail_Param(rules+" is "+rules5_1);
						if (resCode.equals(get_rules_value)) {

							log.info("Packet have all parameter with correct value");
							ver_bean.setSsresp_Param(get_rules_value);
System.out.println("hellloooooooooooooo");
							ver_bean.setTest_status("((*Test Pass*))");
							ver_bean.setTest_Pass_Desc("All required Parameter with expected value Present In request");
							System.out.println("yaya yaha par  2");
							log.info("***--########((((Test Pass))))########--***");
							// log.info("isbot=true, "+rules5_1+" = true  and response code = "
							// +get_rules_value);
						} else {
							log.info("***--((((Test Fail))))--***");
							ver_bean.setTest_status("((*Fail*))");
							ver_bean.setTest_Fail_Desc("Response value not matched with set value in H:sid:sid");
							System.out.println("yaya yaha par  3");
						}
					} else {
						log.info("***--((((Test Fail))))--***");
						ver_bean.setTest_status("((*Fail*))");
						ver_bean.setTest_Fail_Desc("isBot or rules failed value is not true in packet for Active mode");
						System.out.println("yaya yaha par  4");
					}
				} else {
					log.info("***--((((Test Fail))))--***");
					ver_bean.setTest_status("((*Fail*))");
					ver_bean.setTest_Fail_Desc("Required Parameter Not Present in Request");
					System.out.println("yaya yaha par  5");
				}
			
		}
		log.info("");
		log.info("***Deleting apilist in CFM***");
		jedis.del("apilist");
		log.info("'apilist' is deleted");
		/*
		 * CSV_Generator cg=new CSV_Generator(); cg.cfvread(ver_bean);
		 */

	}

	public void calType6(String lis) throws ParseException {
		log.info("Packet Contain CallType 6 then vertx will not check keys");

	}
}
