package com.shieldsquare.vertx.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.shieldsquare.vertx.model.VertxBean;
import com.shieldsquare.vertx.utils.Final_Data;

import redis.clients.jedis.Jedis;

public class Create_Key {
	/*
	 * K:_zpsbd7:sid:ip:uavalue K:_zpsbd5:sid:ip:sessionvalue
	 * K:__uzma:sid:ip:uzmavalue K:__uzmc :sid:ip:uzmcvalue
	 */
	Logger log;

	public Create_Key(Logger log2) {
		// TODO Auto-generated constructor stub
		log = log2;
	}

	public void mrm_Keys_Creation(String apiparameter, int sid, String ip,
			String caught_value, String rules, VertxBean ver_bean) {
		log.info("*Inside keys Preparation method for Create new keys*");
		String key = "K:" + apiparameter + ":" + sid + ":" + ip + ":"
				+ caught_value;
		log.info("Prepared keys is going to Create in MRM is  -->> " + key);
		String key_Stat = keys_Create_MRM(key, rules);

		if (key_Stat.equalsIgnoreCase("OK")) {
			ver_bean.setCreated_Key(key);
		} else {
			ver_bean.setCreated_Key("Keys not created");
		}

	}

	// K:_zpsbd6, K:_zpsbd5, K:_zpsbd9
	// K:_zpsbd9:sid:ud
	// K:_zpsbd6:sid:ip
	// K:_zpsbd5:sid:sessionvalue
	// caught value also can we IPaddress

	public void mrm_Keys_Creation(String apiparameter, int sid,
			String caught_value, String rules, VertxBean ver_bean) {
		log.info("*Inside keys Preparation method for Create new keys*");
		String key = "K:" + apiparameter + ":" + sid + ":" + caught_value;
		log.info("Prepared keys is going to Create in MRM is  -->> " + key);
		String key_Stat = keys_Create_MRM(key, rules);

		if (key_Stat.equalsIgnoreCase("OK")) {
			ver_bean.setCreated_Key(key);
		} else {
			ver_bean.setCreated_Key("Keys not created");
		}
	}

	// K:_zpsbd9:ud.com

	public void mrm_Keys_Creation(String apiparameter, String domain_name,
			String rules,  VertxBean ver_bean) {
		log.info("*Inside keys Preparation method for Create new keys*");
		String key = "K:" + apiparameter + ":" + domain_name;
		log.info("Prepared keys is going to Create in MRM is  -->> " + key);
		String key_Stat = keys_Create_MRM(key, rules);

		if (key_Stat.equalsIgnoreCase("OK")) {
			ver_bean.setCreated_Key(key);
		} else {
			ver_bean.setCreated_Key("Keys not created");
		}
	}

	public String keys_Create_MRM(String key, String rules_value) {
		String key_val = null;
		try {
			log.info("*INSIDE KEYS CREATION METHOD");
			Jedis jedis = new Jedis("104.239.157.169");
			jedis.auth("redis@123@Azure");
			key_val = jedis.setex(key, Final_Data.EXPIRE_TIME, rules_value);

			if (key_val.equalsIgnoreCase("OK")) {
				log.info("Keys Created  with  " + rules_value
						+ " and Expire time is " + Final_Data.EXPIRE_TIME
						+ "  Redis return status for keys is " + key_val);
				log.info("Final Keys created  " + key + " with " + rules_value);
			} else {
				log.info("Redis Return Status for Keys is " + key_val
						+ "  Keys not Created ");
			}
			log.info("");
		} catch (Exception e) {
			log.error("Error found in key Create method  " + e);
		}
		System.out.println("helo this is keys value" + key_val);
		return key_val;
	}
}
