package com.shieldsquare.vertx.logic;

import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.shieldsquare.vertx.model.VertxBean;

import redis.clients.jedis.Jedis;

public class Delet_Keys {

	/*
	 * // zpsbd9:sid:domain public String del_zpsbd9_SID_Keys(String
	 * zpsbd9_value, int sid, String keys_vlu) {
	 * 
	 * String key = "K:" + keys_vlu + ":" + sid + ":" + zpsbd9_value;
	 * delet_Keys(key, zpsbd9_value); return "domain with SID DELETED";
	 * 
	 * }
	 * 
	 * // zpsbd9:domain public String del_zpsbd9_DomainIDKeys(String
	 * domain_MailId, String keys_vlu) {
	 * 
	 * String key = "K:" + keys_vlu + ":" + domain_MailId; delet_Keys(key,
	 * domain_MailId); return "Domain with oy SID deleted";
	 * 
	 * }
	 * 
	 * // zpsbd6:sid:ip public String del_zpsbd6_Keys(String domain_MailId,
	 * String keys_vlu) {
	 * 
	 * String key = "K:" + keys_vlu + ":" + domain_MailId; delet_Keys(key,
	 * domain_MailId); return "Domain with oy SID deleted";
	 * 
	 * }
	 * 
	 * // zpsbd7:sid:ip:ua public String del_zpsbd7_Keys(String domain_MailId,
	 * String keys_vlu) {
	 * 
	 * String key = "K:" + keys_vlu + ":" + domain_MailId; delet_Keys(key,
	 * domain_MailId); return "Domain with oy SID deleted";
	 * 
	 * } //zpsbd5 public String del_zpsbd5IP_Keys(String domain_MailId, String
	 * keys_vlu) {
	 * 
	 * String key = "K:" + keys_vlu + ":" + domain_MailId; delet_Keys(key,
	 * domain_MailId); return "Domain with oy SID deleted";
	 * 
	 * } //zopsbd5 public String del_zpsbd5_Keys(String domain_MailId, String
	 * keys_vlu) {
	 * 
	 * String key = "K:" + keys_vlu + ":" + domain_MailId; delet_Keys(key,
	 * domain_MailId); return "Domain with oy SID deleted";
	 * 
	 * }
	 */

	Logger log;
	
	public Delet_Keys(Logger log2) {
		// TODO Auto-generated constructor stub
		log=log2;
	}
	

	/*
	 * K:_zpsbd7:sid:ip:uavalue K:_zpsbd5:sid:ip:sessionvalue
	 * K:__uzma:sid:ip:uzmavalue K:__uzmc :sid:ip:uzmcvalue
	 */

	public String keys_deletion(String apiparameter, int sid, String ip,
			String value,VertxBean ver_bean) {
		log.info("*Inside keys Preparation method for delete old keys*");
		String key = "K:" + "*" + ":" + sid + ":" + ip +"*";
		log.info("Key Prepared for search and delete in MRM is  -->>  "+key);
		delet_Keys(key, value,ver_bean);

		return null;
	}

	// K:_zpsbd6, K:_zpsbd5, K:_zpsbd9
	// K:_zpsbd9:sid:ud
	// K:_zpsbd6:sid:ip
	// K:_zpsbd5:sid:sessionvalue

	public String keys_deletion(String apiparameter, int sid, String value,VertxBean ver_bean) {
		log.info("*Inside keys Preparation method for delete old keys*");
		String key = "K:" + "*" + ":" + sid + ":" + value+"*";
		log.info("Key Prepared for search and delete in MRM is  -->> "+key);
		delet_Keys(key, value,ver_bean);

		return value;
	}

	// K:_zpsbd9:ud.com
	public Long keys_deletion(String apiparameter, String domain_name_MailId,VertxBean ver_bean) {
		log.info("*Inside keys Preparation method for delete old keys*");
		String key = "K:" + "*" + ":" + domain_name_MailId+"*";
		log.info("Key Prepared for search and delete in MRM is  -->>  "+key);
		Long dsstaus =delet_Keys(key, domain_name_MailId,ver_bean);

		if(dsstaus==null){
			dsstaus=(long) 0;
		}
		return dsstaus;
	}

	// public String delet_Keys(String key, String domain_MailId) {
	public Long delet_Keys(String key, String value, VertxBean ver_bean) {
		Long dStatus = null;
		try{
			
		log.info("*INSDIE DELETE METHOD*");
		Jedis jedis = new Jedis("104.239.157.169");
		jedis.auth("redis@123@Azure");
		
		/*Jedis jedis = new Jedis("104.239.157.67");
		jedis.auth("redis@123@Azure");
*/
		Set<String> set2 = jedis.keys(key);
log.info("Checking Keys in MRM and if Present then key will delete");
		Iterator itt = set2.iterator();
		while (itt.hasNext()) {
			
			String keys = (String) itt.next();
			ver_bean.setOld_Keys_Del(keys);
			System.out.println(keys);
			dStatus=jedis.del(keys);
			log.info("Redis Return Status is "+ dStatus  + "  Keys Deleted   -->>"+keys);
			
			/*if (keys.contains(value)) {
				log.info("Keys Found   -->> "+keys);
				dStatus=jedis.del(keys);
				log.info("Redis Return Status is "+ dStatus  + "  Keys Deleted   -->>"+keys);
			}else{
				log.info("huRReyy No keys Found");
			}*/
		}
		log.info("**Keys Deletion Done**");
		log.info("");
		
		}catch(Exception e){
			log.error("Error found in key Delete method" + e);
		}
		return dStatus;

	}

}
