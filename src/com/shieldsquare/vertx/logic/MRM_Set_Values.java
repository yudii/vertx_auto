package com.shieldsquare.vertx.logic;

import org.apache.log4j.Logger;

import com.shieldsquare.vertx.model.VertxBean;

import redis.clients.jedis.Jedis;

public class MRM_Set_Values {
	
	Logger log;
	public MRM_Set_Values(Logger log2) {
		// TODO Auto-generated constructor stub
		log=log2;
	}
	
	public String rules_Value_MRM(int sid,String hasvalue,VertxBean ver_bean) {
		String hash_Value = null;
		try{
		log.info("*INSIDE RULES VALUE METHOD this method get result from H:sid:<SID>*");
		Jedis jedis = new Jedis("104.239.157.169");
		jedis.auth("redis@123@Azure");	
		
		String hasName="H:sid:"+sid;
		hash_Value=jedis.hget(hasName, hasvalue);
	//	ver_bean.setCust_config_rules(hash_Value);
	//	ver_bean.setEmail_Domain_Check(hash_Value);
		
		log.info("Value in H:sid:"+sid +" is "+ hash_Value+" for "+hasvalue);
		}catch(Exception e){
			log.info("Exception in Rules_value method (H:sid:SID)");
		}
		
		return hash_Value;
	
	}
	
}
