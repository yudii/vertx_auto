package com.shieldsquare.vertx.model;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class VertxBean {

	private int sid;
	private String ip_address;
	private String test_case;
	private String rules;
	private String created_Key;
	private Long del_old_key_stat;
	private String old_Keys_Del;
	public String getOld_Keys_Del() {
		return old_Keys_Del;
	}


	public void setOld_Keys_Del(String old_Keys_Del) {
		this.old_Keys_Del = old_Keys_Del;
	}




	private String error;
	private String total_req_sent;
	private String sent_req;
	
	private String cust_exSID;
	private String req_PID;
	
	private int req_sentNo;
	private String req_Param;
	
	private String isBot_Param;
	private String recvdTime_Param;
	private String ssresp_Param;
	private String rulsfail_Param;
	
	
	private String test_status;
	public  String test_Pass_Desc;
	public  String test_Fail_Desc;
	
	public static String cust_Mode;
	public String cust_config_rules;
	public String email_Domain_Check;



	public String getEmail_Domain_Check() {
		return email_Domain_Check;
	}


	public void setEmail_Domain_Check(String email_Domain_Check) {
		this.email_Domain_Check = email_Domain_Check;
	}


	public static String getCust_Mode() {
		return cust_Mode;
	}


	public static void setCust_Mode(String cust_Mode) {
		VertxBean.cust_Mode = cust_Mode;
	}

	public String getTest_Pass_Desc() {
		return test_Pass_Desc;
	}


	public void setTest_Pass_Desc(String test_Pass_Desc) {
		this.test_Pass_Desc = test_Pass_Desc;
	}


	public String getTest_Fail_Desc() {
		return test_Fail_Desc;
	}


	public void setTest_Fail_Desc(String test_Fail_Desc) {
		this.test_Fail_Desc = test_Fail_Desc;
	}


	public String getCust_config_rules() {
		return cust_config_rules;
	}


	public void setCust_config_rules(String cust_config_rules) {
		this.cust_config_rules = cust_config_rules;
	}


	public String getTest_status() {
		return test_status;
	}


	public void setTest_status(String test_status) {
		this.test_status = test_status;
	}


	public String getIsBot_Param() {
		return isBot_Param;
	}


	public void setIsBot_Param(String isBot_Param) {
		this.isBot_Param = isBot_Param;
	}


	public String getRecvdTime_Param() {
		return recvdTime_Param;
	}


	public void setRecvdTime_Param(String recvdTime_Param) {
		this.recvdTime_Param = recvdTime_Param;
	}


	public String getSsresp_Param() {
		return ssresp_Param;
	}


	public void setSsresp_Param(String ssresp_Param) {
		this.ssresp_Param = ssresp_Param;
	}


	public String getRulsfail_Param() {
		return rulsfail_Param;
	}


	public void setRulsfail_Param(String rulsfail_Param) {
		this.rulsfail_Param = rulsfail_Param;
	}


	public String getReq_Param() {
		return req_Param;
	}


	public void setReq_Param(String req_Param) {
		this.req_Param = req_Param;
	}


	public int getReq_sentNo() {
		return req_sentNo;
	}


	public void setReq_sentNo(int req_sentNo) {
		this.req_sentNo = req_sentNo;
	}


	public String getCust_exSID() {
		return cust_exSID;
	}


	public void setCust_exSID(String cust_exSID) {
		this.cust_exSID = cust_exSID;
	}




	public String getReq_PID() {
		return req_PID;
	}


	public void setReq_PID(String req_PID) {
		this.req_PID = req_PID;
	}




	private String status;


	public int getSid() {
		return sid;
	}


	public void setSid(int sid) {
		this.sid = sid;
	}


	public String getIp_address() {
		return ip_address;
	}


	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}


	public String getTest_case() {
		return test_case;
	}


	public void setTest_case(String test_case) {
		this.test_case = test_case;
	}


	public String getRules() {
		return rules;
	}


	public void setRules(String rules) {
		this.rules = rules;
	}


	public String getCreated_Key() {
		return created_Key;
	}


	public void setCreated_Key(String created_Key) {
		this.created_Key = created_Key;
	}


	public Long getDel_old_key_stat() {
		return del_old_key_stat;
	}


	public void setDel_old_key_stat(Long del_old_key_stat) {
		this.del_old_key_stat = del_old_key_stat;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getTotal_req_sent() {
		return total_req_sent;
	}


	public void setTotal_req_sent(String total_req_sent) {
		this.total_req_sent = total_req_sent;
	}


	public String getSent_req() {
		return sent_req;
	}


	public void setSent_req(String sent_req) {
		this.sent_req = sent_req;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
		
}
