package com.shieldsquare.vertx.requestsender;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.shieldsquare.vertx.model.VertxBean;
import com.shieldsquare.vertx.service.SIDwise_Domain_Method_call;
import com.shieldsquare.vertx.utils.Final_Data;

	public class TestCases {
		
		
	
		static int count;
			
		String status;
		String cust_id_1;
		String cust_Pid_1;
		String referrer_3 = "www.home.com";
		String url_4 = "http://www.bloomberg.com";
		String session_5="only-session";
		String useragent_7 = Final_Data.GOOD_UA;
		String callType_8 = "1";
		String userId_9;
		String uzma="573b91f6d448e4.31900828";
		String uzmc="125471064289";
		String server_cfmIP="104.239.157.67";

		Logger log;	
		public TestCases(Logger log2) {
			// TODO Auto-generated constructor stub
			log=log2;
		}

		public void sendPacket(String IP_add, int sid, String value,VertxBean ver_bean) {
			java.util.Random r=new java.util.Random();

		// ver_bean=new VertxBean();
			
			log.info("**Preparing request to send**");
			
			if(value.contains("domain")){
				userId_9=value;
			log.info("This is 'domain Test-Case Request' zpsbd9 value set as  -->>  " + userId_9);
			
				
			}else if(value.contains("session")){
				session_5=value;
				log.info("This is 'Session Test-Case Request' zpsbd5 value set as  -->> " + session_5);
			}
			else if(value.contains(Final_Data.BAD_UA_1)){
				useragent_7=Final_Data.BAD_UA_1;
				log.info("This is 'Useragent Test-Case Request' zpsbd7 value set as  -->> " + useragent_7);
			}
			
			if(sid==Final_Data.ACTIVE_SID){
				status="true";
				cust_id_1="a9dfb458-ba50-2496-2ab3-56c387484125";
				cust_Pid_1="vertxactive45-2ab3-34b9-23at-gt35";
			log.info("Request for Active Mode Customer zpsbd0 value is set -->> "+status);
			log.info("zpsbd1 is -->>  "+cust_id_1);
			log.info("customer PID is -->>  "+cust_Pid_1);
			
			ver_bean.setCust_exSID(cust_id_1);
			ver_bean.setReq_PID(cust_Pid_1);
				
		System.out.println("Active SID__________________------------------------");
			}else if(sid==Final_Data.MONITOR_SID){
				status="false";
				cust_id_1="509b4b63-492d-4871-bcca-29744a090034";
				cust_Pid_1="vertxmonitor45-2ab3-34b9-23at-gt35";
				log.info("Request for Monitor Mode Customer zpsbd0 value is set -->> "+status);
				log.info("zpsbd1 is -->>  "+cust_id_1);
				log.info("customer PID is -->>  "+cust_Pid_1);
				
				ver_bean.setCust_exSID(cust_id_1);
				ver_bean.setReq_PID(cust_Pid_1);
			}
			
			for (int i = 1; i <= 1; i++) {

				ObjectMapper mapper = new ObjectMapper();
				ObjectNode ssreqJsonObj = mapper.createObjectNode();
				String reqIpAddr = null;
			//	String shieldsquare_tid = shieldsquare_generate_tid(reqIpAddr, cust_id_1);
				ssreqJsonObj.put("_zpsbd0", status);
				ssreqJsonObj.put("_zpsbd1", cust_id_1);
				//ssreqJsonObj.put("_zpsbd2", shieldsquare_tid);
				ssreqJsonObj.put("_zpsbd2", cust_Pid_1 +r.nextInt());
				ssreqJsonObj.put("_zpsbd3", referrer_3);
				ssreqJsonObj.put("_zpsbd4", url_4);
				ssreqJsonObj.put("_zpsbd5", session_5);
				ssreqJsonObj.put("_zpsbd7", useragent_7);
				ssreqJsonObj.put("_zpsbd8", 1);
				ssreqJsonObj.put("_zpsbd9", userId_9);

				// server time in UNIX timestamp
				ssreqJsonObj.put("_zpsbda", System.currentTimeMillis() / 1000);

				// HTTP cookie session
				ssreqJsonObj.put("__uzma", uzma);
				//
				ssreqJsonObj.put("__uzmb", 0);
				// ssreqJsonObj.put("_zpsbd6", testIP);
				// HTTP cookie identifying the number of pages
				ssreqJsonObj.put("__uzmc", uzmc);

				//
				ssreqJsonObj.put("__uzmd", 0);
				boolean flag = true;
				// boolean flag=isValidIP(testIP);
				if (flag) {
					ssreqJsonObj.put("_zpsbd6", IP_add);
					System.out.println("Sending Request .....");
					count++;
					ver_bean.setReq_sentNo(count);
					System.out.println("Total request count = " + count);
					
					log.info("Sending Request...... count is  " +count);
					try {
						String	url = server_cfmIP;
						// String url="http://localhost:7070//getRequestData";
						ObjectNode ssrespJson = sync_sendReq2SS(url,
								ssreqJsonObj.toString());
						
						System.out
								.println("Done here .......||||||.......\n request = "
										+ ssreqJsonObj
										+ "\n response = "
										+ ssrespJson);
						
						
						log.info("packet is "+ ssreqJsonObj );
						log.info("response  " + ssrespJson  );
					
						/*String req=ssreqJsonObj.toString();
						String res=ssrespJson.toString();		
						ver_bean.setReq(req);
						ver_bean.setReq_response(res);
*/						

					} catch (Exception e) {
						System.out.println("Exception is " + e);
						log.info("exception found in sending request  "+e);
					}
				}
			}
			
			SIDwise_Domain_Method_call av=new SIDwise_Domain_Method_call();
			av.beanhigh.add(ver_bean);
		}

		public ObjectNode sync_sendReq2SS(String ss2url, String indata)
				throws Exception {
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode ss2respJson = mapper.createObjectNode();
			HttpURLConnection connection = null;
			DataOutputStream out = null;
			BufferedInputStream inStream = null;
			BufferedReader respReader = null;
			try {
				URL url = new URL("http://" + ss2url + "/getRequestData");
				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setDoInput(true);
				connection.setRequestMethod("POST");
				//connection.setConnectTimeout(ConnConstants.REQ_TIME_OUT);
				//connection.setReadTimeout(ConnConstants.REQ_TIME_OUT);
			//	connection.setUseCaches(false);
				connection.setRequestProperty("Content-type", "application/json");
				connection.setRequestProperty("Content-Encoding", "UTF-8");
				out = new DataOutputStream(connection.getOutputStream());
				out.writeBytes(URLEncoder.encode(indata, "UTF-8"));
				out.flush();
				connection.connect();
				
				inStream = new BufferedInputStream(connection.getInputStream());

				int statusCode = connection.getResponseCode();
				if (statusCode != 200) {
					inStream = new BufferedInputStream(connection.getErrorStream());
				}

				// read the output from the server
				respReader = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
				StringBuilder respBuilder = new StringBuilder();

				String line = null;
				while ((line = respReader.readLine()) != null) {
					respBuilder.append(line + "\n");
				}

				String ssresp = respBuilder.toString();
				JsonNode respJsonNode = (ObjectNode) mapper.readTree(ssresp);
				if (respJsonNode.isObject()) {
					ss2respJson = (ObjectNode) respJsonNode;
				}
			} catch (IOException e) {
				throw e;
			} catch (Exception e) {
				throw e;
			} finally {
				try {
					if (out != null)
						out.close();
				} catch (IOException e) {
					// ignore.
				}

				try {
					if (inStream != null)
						inStream.close();
				} catch (IOException e) {
					// ignore.
				}

				try {
					if (respReader != null)
						respReader.close();
				} catch (IOException ioe) {
					// ignore
				}

			}
			return ss2respJson;
		}
		
	}
