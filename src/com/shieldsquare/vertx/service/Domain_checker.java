/**
 * @version V1.2
 * @author yudi
 */



package com.shieldsquare.vertx.service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;

import com.shieldsquare.vertx.logic.CFMData_Check;
import com.shieldsquare.vertx.logic.Create_Key;
import com.shieldsquare.vertx.logic.Delet_Keys;
import com.shieldsquare.vertx.logic.MRM_Set_Values;
import com.shieldsquare.vertx.model.VertxBean;
import com.shieldsquare.vertx.requestsender.TestCases;
import com.shieldsquare.vertx.utils.CSV_Generator;
import com.shieldsquare.vertx.utils.Final_Data;

public class Domain_checker {

	Logger log;

	public Domain_checker(Logger logger) {
		// TODO Auto-generated constructor stub
		log = logger;
	}

	// K:_zpsbd9:<domain_name of mail ID>
	// emaildomaincheck
	// use property file for that value check parsml code
	
	CSV_Generator cg=new CSV_Generator();
	public void email_Domain_Name_of_Mail(int sid, ArrayList<VertxBean> beanhigh)
			throws ParseException, IOException {

		VertxBean ver_bean = new VertxBean();

		log.info("<<<<<<Inside Email-Domain-Check Method>>>>>>>");
		log.info("****Checking and Deleting for All Present Domain keys**** ");
		System.out.println("HellooooooooooooooooooDomainchecker");

		Delet_Keys dk = new Delet_Keys(log);
		dk.keys_deletion(Final_Data.API_PARAMETER_9,
				Final_Data.DOMAIN_of_MAIL_ID,ver_bean);

		// ver_bean.setKeys_deletion(del_keyStat);

		log.info("****Keys Creation for Domain Keys in MRM**** ");

		ver_bean.setSid(sid);
		ver_bean.setTest_case("Domain Test Case ----> (K:_zpsbd9:<domain_name of mail ID>");
		ver_bean.setIp_address(Final_Data.NORMAL_IP);
		ver_bean.setRules(Final_Data.RULES_FAILED_1);
		
		Create_Key ck = new Create_Key(log);
		ck.mrm_Keys_Creation(Final_Data.API_PARAMETER_9,
				Final_Data.DOMAIN_of_MAIL_ID, Final_Data.RULES_FAILED_1,
				ver_bean);

		log.info("***checking EMAIL-DOMAIN-CHECK is enabled for customer***");
		MRM_Set_Values msValue = new MRM_Set_Values(log);

		
		
		String edcvalue=null;
		if ((edcvalue=msValue.rules_Value_MRM(sid, Final_Data.HASH_VALUE,ver_bean)) != null
				&& "1".equals(edcvalue=msValue.rules_Value_MRM(sid,
						Final_Data.HASH_VALUE,ver_bean))) {
ver_bean.setEmail_Domain_Check(edcvalue);
			log.info("Email-Domain-Check Present for SID and Hash-Return Value is -->> 1");
			log.info("");

			log.info("****Calling Request Send Method****");

			TestCases t = new TestCases(log);
			t.sendPacket(Final_Data.NORMAL_IP, sid, Final_Data.ZPSBD9_VALUE,
					ver_bean);
			CFMData_Check cfmD_Check = new CFMData_Check(log);

			log.info("");
			log.info("****Calling CFM Data Check method****");
			if (sid == Final_Data.ACTIVE_SID) {
				System.out.println(Final_Data.RULES_FAILED_1
						+ "-------------------------------------");
				cfmD_Check.active_ModeData(Final_Data.RULES_FAILED_1, sid,
						ver_bean);

			} else if (sid == Final_Data.MONITOR_SID) {
				cfmD_Check.monitor_ModeData(Final_Data.RULES_FAILED_1, sid,ver_bean);
			}

		} else {
			log.info("Emaildomain Not Present for" + sid
					+ " and HashReturn Value is 0 This case is not applicable");
			ver_bean.setTest_Fail_Desc("Email Domain check not present for SID Case Not appilicable Here");
		}
		beanhigh.add(ver_bean);
		//cg.cfvread(beanhigh);
	}

	// _____________________________________________________________________________________________________________________________________-

	// K:_zpsbd9:<Internal_SID>:<User name value>

	public void username_Value_With_SID(int sid, ArrayList<VertxBean> beanhigh)
			throws ParseException, IOException {

		log.info("<<<<<<Inside Email-Domain-Check Method With SID>>>>>>>");
		log.info("****Checking and Deleting for All Present Domain keys**** ");
		VertxBean ver_bean = new VertxBean();
		ver_bean.setSid(sid);
		ver_bean.setTest_case("Domain with SID  ----> (K:_zpsbd9:<Internal_SID>:<User name value>");
		ver_bean.setIp_address(Final_Data.NORMAL_IP);
		ver_bean.setRules(Final_Data.RULES_FAILED_1);
		
		
		Delet_Keys dk = new Delet_Keys(log);
		dk.keys_deletion(Final_Data.API_PARAMETER_9, sid,
				Final_Data.ZPSBD9_VALUE,ver_bean);

		log.info("****Keys Creation for Domain Keys in MRM**** ");
		Create_Key ck = new Create_Key(log);
		ck.mrm_Keys_Creation(Final_Data.API_PARAMETER_9, sid,
				Final_Data.ZPSBD9_VALUE, Final_Data.RULES_FAILED_1, ver_bean);

		log.info("***checking EMAIL-DOMAIN-CHECK is enabled for customer or not***");
		String edcvalue=null;
		MRM_Set_Values msValue = new MRM_Set_Values(log);
		if ("0".equals(edcvalue=msValue.rules_Value_MRM(sid, Final_Data.HASH_VALUE,ver_bean))
				|| (edcvalue=msValue.rules_Value_MRM(sid, Final_Data.HASH_VALUE,ver_bean)) == null) {
			log.info("Email-Domain-Check not Present for SID and Hash-Return Value is -->> 0 or Null");
			log.info("");

			//ver_bean.setEmail_Domain_Check(edcvalue);
			log.info("****Calling Request Send Method****");
			TestCases t = new TestCases(log);
			// catch IP from JSP form updatelater
			t.sendPacket(Final_Data.NORMAL_IP, sid, Final_Data.ZPSBD9_VALUE,
					ver_bean);

			log.info("");
			log.info("****Calling CFM Data Check method****");

			CFMData_Check cfmD_Check = new CFMData_Check(log);
			if (sid == Final_Data.ACTIVE_SID) {
				cfmD_Check.active_ModeData(Final_Data.RULES_FAILED_1, sid,
						ver_bean);

			} else if (sid == Final_Data.MONITOR_SID) {
				cfmD_Check.monitor_ModeData(Final_Data.RULES_FAILED_1, sid,ver_bean);
			}
			

		} else {
			log.info("Emaildomain present for" + sid
					+ " HashReturn Value is 1 This case Not applicable");
			log.info("");
			ver_bean.setTest_Fail_Desc("Email Domain check present(value 1) for SID Case Not appilicable Here");
		}
		beanhigh.add(ver_bean);
		//cg.cfvread(beanhigh);
	}

	// K:_zpsbd5:<Internal_SID>:<session value>

	public void session_Value_Withoout_IP(int sid, ArrayList<VertxBean> beanhigh)
			throws ParseException, IOException {
		log.info("<<<<<<Inside Session Keys Method With SID>>>>>>>");
		log.info("****Checking and Deleting for All Present Session keys**** ");
		VertxBean ver_bean = new VertxBean();
		ver_bean.setSid(sid);
		ver_bean.setTest_case("Session Test ----> (K:_zpsbd5:<Internal_SID>:<session value>");
		ver_bean.setIp_address(Final_Data.NORMAL_IP);
		ver_bean.setRules(Final_Data.RULES_FAILED_1);
		
		
		Delet_Keys dk = new Delet_Keys(log);

		dk.keys_deletion(Final_Data.API_PARAMETER_5, sid,
				Final_Data.ZPSBD5_VALUE,ver_bean);

		log.info("****Keys Creation for Session Keys in MRM**** ");
		Create_Key ck = new Create_Key(log);
		ck.mrm_Keys_Creation(Final_Data.API_PARAMETER_5, sid,
				Final_Data.ZPSBD5_VALUE, Final_Data.RULES_FAILED_1, ver_bean);

		log.info("");
		log.info("****Calling Request Send Method****");
		TestCases t = new TestCases(log);
		// catch IP from JSP form updatelater
		t.sendPacket(Final_Data.NORMAL_IP, sid, Final_Data.ZPSBD5_VALUE,
				ver_bean);
		CFMData_Check cfmD_Check = new CFMData_Check(log);
		log.info("");
		log.info("****Calling CFM Data Check method****");
		if (sid == Final_Data.ACTIVE_SID) {
			cfmD_Check
					.active_ModeData(Final_Data.RULES_FAILED_1, sid, ver_bean);

		} else if (sid == Final_Data.MONITOR_SID) {
			cfmD_Check.monitor_ModeData(Final_Data.RULES_FAILED_1, sid,ver_bean);
		}
		beanhigh.add(ver_bean);
		
	//	cg.cfvread(beanhigh);
	}

	// K:_zpsbd5:<Internal_SID>:<IP>:<session value>

	public void session_Value_With_IP(int sid, ArrayList<VertxBean> beanhigh)
			throws ParseException, IOException {
		log.info("<<<<<<Inside Session Keys Method With IP and SID>>>>>>>");

		String ip_add = null;
		List<String> rules_Key = new ArrayList<String>();
		rules_Key.add("r1");
		rules_Key.add("r2");

		for (int i = 0; i <= rules_Key.size() - 1; i++) {
			String rules_failed = rules_Key.get(i);
			VertxBean ver_bean = new VertxBean();
			
			ver_bean.setSid(sid);
			ver_bean.setTest_case("Session with IP Case ----> (K:_zpsbd5:<Internal_SID>:<session value>");
			

			if (rules_failed.equalsIgnoreCase(Final_Data.RULES_FAILED_2)) {
				ip_add = Final_Data.DC_IP;
				
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			} else if (rules_failed.equalsIgnoreCase(Final_Data.RULES_FAILED_1)) {
				ip_add = Final_Data.NORMAL_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			}

			if (rules_failed.equalsIgnoreCase("r1")) {
				log.info("**Case 1st for 'Session with IP Keys' with  "
						+ rules_failed + " **");
				log.info("*Test for " + rules_failed
						+ "  Rules and  Normal IP  " + ip_add + "**** ");
			} else {
				log.info("**Case 2nd for 'Session with IP Keys' with  "
						+ rules_failed + " **");
				log.info("*Test for " + rules_failed + " Rules and  DC IP "
						+ ip_add + "**** ");
			}
			log.info("");
			log.info("****Checking and Deleting for All Present Session keys**** ");
			Delet_Keys dk = new Delet_Keys(log);
			dk.keys_deletion(Final_Data.API_PARAMETER_5, sid, ip_add,
					Final_Data.ZPSBD5_VALUE2,ver_bean);

			log.info("****Keys Creation for Session Keys in MRM**** ");
			Create_Key ck = new Create_Key(log);
			ck.mrm_Keys_Creation(Final_Data.API_PARAMETER_5, sid, ip_add,
					Final_Data.ZPSBD5_VALUE2, rules_failed, ver_bean);

			log.info("");
			log.info("****Calling Request Send Method****");
			TestCases t = new TestCases(log);
			// catch IP from JSP form updatelater
			t.sendPacket(ip_add, sid, Final_Data.ZPSBD5_VALUE2, ver_bean);
			CFMData_Check cfmD_Check = new CFMData_Check(log);
			log.info("");
			log.info("****Calling CFM Data Check method****");
			if (sid == Final_Data.ACTIVE_SID) {
				cfmD_Check.active_ModeData(rules_failed, sid, ver_bean);

			} else if (sid == Final_Data.MONITOR_SID) {
				cfmD_Check.monitor_ModeData(rules_failed, sid,ver_bean);
			}
			beanhigh.add(ver_bean);
		}
	//	cg.cfvread(beanhigh);
	}
}
