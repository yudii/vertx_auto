/**
@author yogesh:
*/

package com.shieldsquare.vertx.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.simple.parser.ParseException;

import com.shieldsquare.vertx.logic.CFMData_Check;
import com.shieldsquare.vertx.requestsender.TestCases;

import redis.clients.jedis.Jedis;

public class KeysCreationLogic {

	/*	public void keys_MRM() throws ParseException {
		
		System.out.println("***********Vertx Testing Start**********");
		System.out.println();
		Jedis jedis = new Jedis("104.239.157.169");
		jedis.auth("redis@123@Azure");
		String IP_add = "121.121.121.121";
		// int sid = 19;
		
	System.out.println("SID will be use - 19 for Active mode and 1907 for Monitor Mode");
	System.out.println("Expire Time for Keys will be 1200");	
	System.out.println();
	
	
		int expire_time = 1200;
		List<Integer> sid_List = new ArrayList<Integer>();
		sid_List.add(19);
		sid_List.add(1907);

		System.out.println("Rules will be verified ");
		System.out.println("For _zpsbd6 - r4, r6, r10");
		System.out.println("For _zpsbd6 - r1, r2, r5");
		System.out.println();
		
		
		List<String> rules_Key = new ArrayList<String>();
		 rules_Key.add("r1");
		 rules_Key.add("r2");
		 rules_Key.add("r4");
		 rules_Key.add("r5");
		 rules_Key.add("r6");
		 rules_Key.add("r10");
		rules_Key.add("uzmar1");
		rules_Key.add("uzmar2");
		rules_Key.add("uzmcr1");
		rules_Key.add("uzmcr2");
		
		for (int j = 0; j <= sid_List.size()-1; j++) {

			int sid = sid_List.get(j);
			System.out.println(sid);
			
System.out.println("________________________Tets Start__________________________________________");
			for (int i = 0; i <= rules_Key.size() - 1; i++) {

				String rules_value = rules_Key.get(i);
				System.out.println("IP for Test is " + IP_add);
				System.out.println("Rules for Key is " + rules_value);
		/*		System.out.println("================================================================================================================================================================");
System.out.println("*************************************************8");


				Set<String> set2 = jedis
						.keys("K:_zpsbd6:" + sid + ":" + IP_add);
				Set<String> set2 = jedis.keys("K:_zpsbd6:" + sid + ":" + IP_add);
				// Set<String>set2=jedis.keys("K:_zpsbd7:"+sid+":"+ip+":Java");

				System.out.println("Checking Keys in MRM and if Present then key will delete");
				Iterator itt = set2.iterator();
				while (itt.hasNext()) {
					String keys = (String) itt.next();
					if (keys.contains(IP_add)) {
						jedis.del(keys);
						System.out.println("Keys is DELETED " + keys);
					}
				}
				System.out.println();
				
			
				System.out.println("Blacklisting keys for rules");
				System.out.println(IP_add+"---This IP will blacklist with---- " + rules_value);
				// String key ="K:_zpsbd7:"+sid+":"+ip+":Java";

				if (rules_value == "r1" || rules_value == "r2"
						|| rules_value == "r5") {
					String key = "K:_zpsbd7:" + sid + ":" + IP_add + ":java";
					jedis.setex(key, expire_time, rules_value);
				}
				if (rules_value == "r4" || rules_value == "r6"
						|| rules_value == "r10") {
					String key = "K:_zpsbd6:" + sid + ":" + IP_add;
					jedis.setex(key, expire_time, rules_value);
				}
				
		//Uzma and uzmc keys key creation		
				
				String uzma="573b91f6d448e4.31900828";
				if (rules_value == "uzmar1"){ 
					String r1="r1";

					String key = "K:__uzma:" + sid + ":" + IP_add +":"+uzma;
							jedis.setex(key, expire_time, r1);
							System.out.println("Created Keys is for UZMA r1      --  "+key);
							System.out.println();
					
				}
				if(rules_value == "uzmar2") {
					String r2="r2";
					String key = "K:__uzma:" + sid + ":" + IP_add +":"+uzma;
			//		System.out.println("09009009090090909099000909009"+key);
		/*			jedis.setex(key, expire_time, r2);
					System.out.println("Created Keys is for UZMA r2      --  "+key);
					System.out.println();
				}
				
				if (rules_value == "uzmac1"){
					
					String r1="r1";

					String key = "K:__uzmc:" + sid + ":" + IP_add +":"+10;
							jedis.setex(key, expire_time, r1);
							System.out.println("Created Keys is for UZMC r1      --  "+key);
							System.out.println();

					
					
				} if(rules_value == "uzmac2") {

					String r2="r2";

					String key = "K:__uzmc:" + sid + ":" + IP_add +":"+10;
							jedis.setex(key, expire_time, r2);
							System.out.println("Created Keys is for UZMC r2      --  "+key);
							System.out.println();
				}

		System.out.println("Sending request to SS server");
		System.out.println();
				TestCases t = new TestCases();
				t.sendPacket(IP_add, sid);

				System.out.println("____________________________________________________________________");
				System.out.println("Start Checking data in CFM ");
				
				CFMData_Check cfmc = new CFMData_Check();
				if (sid == 1907) {

					cfmc.monitor_ModeData(rules_value, sid);
				}else if(sid == 19){
					cfmc.active_ModeData(rules_value, sid);
		
				}
				
//System.out.println("");
			}
		}
		//System.out.println("FInally DONEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
	}

	public static void main(String[] args) throws ParseException {

		KeysCreationLogic on = new KeysCreationLogic();
		on.keys_MRM();

	}
*/
}
