package com.shieldsquare.vertx.service;

/**
 * @version V1.2
 * @author yudi
 */

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.shieldsquare.vertx.logic.CFMData_Check;
import com.shieldsquare.vertx.logic.Create_Key;
import com.shieldsquare.vertx.logic.Delet_Keys;
import com.shieldsquare.vertx.logic.MRM_Set_Values;
import com.shieldsquare.vertx.model.VertxBean;
import com.shieldsquare.vertx.requestsender.TestCases;
import com.shieldsquare.vertx.utils.CSV_Generator;
import com.shieldsquare.vertx.utils.Final_Data;


public class Main_Keys_Checker {

	int sid=19;
	String ip_add;
	String rules="r1";
	
	Logger log;
	public Main_Keys_Checker(Logger logger) {
		// TODO Auto-generated constructor stub
		log=logger;
	}
	CSV_Generator cg=new CSV_Generator();
	public void ip_Keys(int sid, ArrayList<VertxBean> beanhigh) throws IOException {
		log.info("<<<<<<Inside IP-Keys-Check Method>>>>>>>");
	
		try{
		ArrayList<String>arr=new ArrayList<String>();
				arr.add("r2");
				arr.add("r4");
				arr.add("r10");
		
	for(int i=0; i <=arr.size()-1; i++){
		String rules_failed=arr.get(i);			
		
		VertxBean ver_bean=new VertxBean();
		ver_bean.setSid(sid);
		ver_bean.setTest_case("IP Test Case ----> (K:_zpsbd6:SID:IP)");
		
		
		if(rules_failed.equalsIgnoreCase("r2")){
			ip_add=Final_Data.DC_IP;
			ver_bean.setIp_address(ip_add);
			ver_bean.setRules(rules_failed);
		}
		else if(rules_failed.equalsIgnoreCase("r4")){
			ip_add=Final_Data.AGG_CRAW_IP;
			ver_bean.setIp_address(ip_add);
			ver_bean.setRules(rules_failed);
		}
		else if(rules_failed.equalsIgnoreCase("r10")){
			ip_add=Final_Data.NORMAL_IP;
			ver_bean.setIp_address(ip_add);
			ver_bean.setRules(rules_failed);
		}
		
//we can add these condition in one
		if(rules_failed.equalsIgnoreCase("r2")){
			log.info("**Case 1st for ' IP Keys' with  "+rules_failed +" **");
			log.info("*Test for "+rules_failed +"  Rules and  DC IP  "+ ip_add +"**** ");
		//	ver_bean.setRules(rules_failed);
		}else if(rules_failed.equalsIgnoreCase("r4")){
			log.info("**Case 2nd for ' IP Keys' with  "+rules_failed +" **");
			log.info("*Test for "+rules_failed +" Rules and  Aggregator IP "+ip_add+"**** ");
			//ver_bean.setRules(rules_failed);
		} else {
			log.info("**Case 3rd for ' IP Keys' with  "+rules_failed +" **");
			log.info("*Test for "+rules_failed +" Rules and  Normal IP "+ip_add+"**** ");
			ver_bean.setRules(rules_failed);
		}

		// Keys Check and delete
		log.info("");
		log.info("****Checking and Deleting for All Present Domain keys**** ");
				Delet_Keys del_key = new Delet_Keys(log);
				del_key.keys_deletion(Final_Data.API_PARAMETER_6, sid, ip_add,ver_bean);
		
				
				log.info("****Keys Creation for IP Keys in MRM**** ");
				Create_Key create_key = new Create_Key(log);				
				create_key.mrm_Keys_Creation(Final_Data.API_PARAMETER_6, sid, ip_add, rules_failed,ver_bean);
				
				
				log.info("");
				log.info("****Calling Request Send Method****");
					TestCases t = new TestCases(log);
					
					t.sendPacket(ip_add, sid, Final_Data.API_PARAMETER_6,ver_bean);
					CFMData_Check cfmD_Check = new CFMData_Check(log);

					
					log.info("");
					log.info("****Calling CFM Data Check method****");
					if (sid == Final_Data.ACTIVE_SID) {
						cfmD_Check.active_ModeData(rules_failed, sid,ver_bean);
					} else if(sid == Final_Data.MONITOR_SID){
						
						cfmD_Check.monitor_ModeData(rules_failed, sid,ver_bean);
					}
					beanhigh.add(ver_bean);
					
				} 
	
		}catch(Exception e){
			e.printStackTrace();
			log.error("Exception in ZPSBD6 KEY CASE"+e);
		}
		
		
		cg.cfvread(beanhigh);
	}

	
	public void useragent_Keys(int sid, ArrayList<VertxBean> beanhigh) throws IOException {

		log.info("<<<<<<Inside Useragent-Keys-Check Method>>>>>>>");
		
		try{
			ArrayList<String>arr=new ArrayList<String>();
					arr.add("r1");
					arr.add("r2");
					arr.add("r5");
			
		for(int i=0; i <=arr.size()-1; i++){
			String rules_failed=arr.get(i);			
			VertxBean ver_bean=new VertxBean();
			
			ver_bean.setSid(sid);
			ver_bean.setTest_case("IP Test Case ----> (K:_zpsbd7:SID:IP:UAValue)");
			if(rules_failed.equalsIgnoreCase("r2")){
				ip_add=Final_Data.DC_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			}
			else if(rules_failed.equalsIgnoreCase("r1")){
				ip_add=Final_Data.NORMAL_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
				
			}else {
				ip_add=Final_Data.NORMAL_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
				
			}			
			
			
			
			if(rules_failed.equalsIgnoreCase("r2")){
				log.info("**Case 2nd for ' Useragent Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +"  Rules and  DC IP  "+ ip_add +"**** ");
			}else if(rules_failed.equalsIgnoreCase("r1")){
				log.info("**Case 1st for ' Useragent Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +" Rules and  Normal IP "+ip_add+"**** ");
			} else {
				log.info("**Case 3rd for ' Useragent Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +" Rules and  Normal IP "+ip_add+"**** ");
			}
			
			
			
			
			
			log.info("");
			log.info("****Checking and Deleting for All Present Useragent keys**** ");
			// Keys Check and delete
					Delet_Keys del_key = new Delet_Keys(log);
					 
				del_key.keys_deletion(Final_Data.API_PARAMETER_7, sid, ip_add,Final_Data.BAD_UA_1,ver_bean);
					
					// keys Creation
					// you can get rules value from file and call create key mthod
					// to create key and pass parameter in crete key
				log.info("****Keys Creation for Useragent Keys in MRM**** ");
					Create_Key create_key = new Create_Key(log);	
					create_key.mrm_Keys_Creation(Final_Data.API_PARAMETER_7, sid, ip_add, Final_Data.BAD_UA_1 ,rules_failed,ver_bean);
					
					log.info("");
					log.info("****Calling Request Send Method****");
					
						TestCases t = new TestCases(log);
						t.sendPacket(ip_add, sid, Final_Data.BAD_UA_1,ver_bean);
						CFMData_Check cfmD_Check = new CFMData_Check(log);
						log.info("");
						log.info("****Calling CFM Data Check method****");
						if (sid == Final_Data.ACTIVE_SID) {
							cfmD_Check.active_ModeData(rules_failed, sid,ver_bean);
						} else if(sid == Final_Data.MONITOR_SID){
							
							cfmD_Check.monitor_ModeData(rules_failed, sid,ver_bean);
						}
						beanhigh.add(ver_bean);
					} 
		
			}catch(Exception e){
				e.printStackTrace();
				log.error("Exception in zpsbd7 KEY CASE"+e);
			}
		cg.cfvread(beanhigh);
	}
	
	
	
	
	
	public void uzma_Keys(int sid, ArrayList<VertxBean> beanhigh) throws IOException {
		log.info("<<<<<<Inside UZMA-Keys-Check Method>>>>>>>");
		
		try{
			ArrayList<String>arr=new ArrayList<String>();
					arr.add("r1");
					arr.add("r2");
			
		for(int i=0; i <=arr.size()-1; i++){
			String rules_failed=arr.get(i);			
			VertxBean ver_bean=new VertxBean();
			
			ver_bean.setSid(sid);
			ver_bean.setTest_case("IP Test Case ----> (K:__UZMA:SID:IP:UZMAValue)");
			
			if(rules_failed.equalsIgnoreCase("r2")){
				ip_add=Final_Data.DC_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			}
			else{
				ip_add=Final_Data.NORMAL_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			}
			
			
			
			
			
			if(rules_failed.equalsIgnoreCase("r1")){
				log.info("**Case 1st for ' UZMA Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +"  Rules and  Normal IP  "+ ip_add +"**** ");
			}else if(rules_failed.equalsIgnoreCase("r2")){
				log.info("**Case 2nd for ' UZMA Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +" Rules and  DC IP "+ip_add+"**** ");
			} 
			
			
			
			
			
			log.info("");
			log.info("****Checking and Deleting for All Present UZMA keys**** ");
			// Keys Check and delete
					Delet_Keys del_key = new Delet_Keys(log);
					 
					del_key.keys_deletion(Final_Data.UZMA, sid, ip_add,Final_Data.UZMA_VALUE,ver_bean);
					
					// keys Creation
					// you can get rules value from file and call create key mthod
					// to create key and pass parameter in crete key
					
				log.info("****Keys Creation for IP Keys in MRM**** ");
					Create_Key create_key = new Create_Key(log);	
					create_key.mrm_Keys_Creation(Final_Data.UZMA, sid, ip_add, Final_Data.UZMA_VALUE ,rules_failed,ver_bean);
					
					log.info("");
					log.info("****Calling Request Send Method****");
						TestCases t = new TestCases(log);
						t.sendPacket(ip_add, sid, Final_Data.UZMA_VALUE,ver_bean);
						CFMData_Check cfmD_Check = new CFMData_Check(log);
					
						log.info("");
						log.info("****Calling CFM Data Check method****");
						if (sid == Final_Data.ACTIVE_SID) {
							cfmD_Check.active_ModeData(rules_failed, sid,ver_bean);
						} else if(sid == Final_Data.MONITOR_SID){
							
							cfmD_Check.monitor_ModeData(rules_failed, sid,ver_bean);
						}
						beanhigh.add(ver_bean);
					} 
		
			}catch(Exception e){
				e.printStackTrace();
				log.error("Exception in UZMA KEY CASE"+e);
			}
		cg.cfvread(beanhigh);
	}

	
	
	//UZMC Keys Test
	public void uzmc_Keys(int sid, ArrayList<VertxBean> beanhigh) throws IOException {

		log.info("<<<<<<Inside UZMC-Keys-Check Method>>>>>>>");
		try{
			ArrayList<String>arr=new ArrayList<String>();
					arr.add("r1");
					arr.add("r2");
					
			
		for(int i=0; i <=arr.size()-1; i++){
			String rules_failed=arr.get(i);			
			VertxBean ver_bean=new VertxBean();
			ver_bean.setSid(sid);
			ver_bean.setTest_case("IP Test Case ----> (K:__UZMC:SID:IP:UZMCValue)");
			
			if(rules_failed.equalsIgnoreCase("r2")){
				ip_add=Final_Data.DC_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			}
			else{
				ip_add=Final_Data.NORMAL_IP;
				ver_bean.setIp_address(ip_add);
				ver_bean.setRules(rules_failed);
			}
			
			if(rules_failed.equalsIgnoreCase("r1")){
				log.info("**Case 1st for ' UZMC Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +"  Rules and  Normal IP  "+ ip_add +"**** ");
			}else if(rules_failed.equalsIgnoreCase("r2")){
				log.info("**Case 2nd for ' UZMC Keys' with  "+rules_failed +" **");
				log.info("*Test for "+rules_failed +" Rules and  DC IP "+ip_add+"**** ");
			}
			
			
			
			log.info("");
			log.info("****Checking and Deleting for All Present uzmc keys**** ");
			// Keys Check and delete
					Delet_Keys del_key = new Delet_Keys(log);
					 
				del_key.keys_deletion(Final_Data.UZMC, sid, ip_add,Final_Data.UZMC_VALUE,ver_bean);
					
					// keys Creation
					// you can get rules value from file and call create key mthod
					// to create key and pass parameter in crete key
				System.out.println(rules_failed+"  rules_failed");
		
				log.info("****Keys Creation for UZMC Keys in MRM**** ");
					Create_Key create_key = new Create_Key(log);	
					create_key.mrm_Keys_Creation(Final_Data.UZMC, sid, ip_add, Final_Data.UZMC_VALUE ,rules_failed,ver_bean);
					
					log.info("");
					log.info("****Calling Request Send Method****");
						TestCases t = new TestCases(log);
						t.sendPacket(ip_add, sid, Final_Data.UZMC_VALUE,ver_bean);
						CFMData_Check cfmD_Check = new CFMData_Check(log);
						log.info("");
						log.info("****Calling CFM Data Check method****");
						if (sid == Final_Data.ACTIVE_SID) {
							cfmD_Check.active_ModeData(rules_failed, sid,ver_bean);
						} else if(sid == Final_Data.MONITOR_SID){
							
							cfmD_Check.monitor_ModeData(rules_failed, sid,ver_bean);
						}
						beanhigh.add(ver_bean);
					} 
		
			}catch(Exception e){
				e.printStackTrace();
				log.error("Exception in UZMC KEY CASE"+e);
			}
		cg.cfvread(beanhigh);
	}
}
