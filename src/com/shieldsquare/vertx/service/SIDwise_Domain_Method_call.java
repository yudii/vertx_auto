package com.shieldsquare.vertx.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;

import com.shieldsquare.vertx.model.VertxBean;
import com.shieldsquare.vertx.utils.CSV_Generator;
import com.shieldsquare.vertx.utils.Final_Data;

public class SIDwise_Domain_Method_call {
	final static Logger logger = Logger.getLogger(SIDwise_Domain_Method_call.class);
	

	public ArrayList<VertxBean>beanhigh=new ArrayList<VertxBean>();
	
	VertxBean vbean=new VertxBean();
	public void method_call() throws ParseException{
		
		
		logger.info("__________________________Testing Start__________________________");
		Domain_checker dc=new Domain_checker(logger);
		Main_Keys_Checker mkc=new Main_Keys_Checker(logger);
		List<Integer> sid_List = new ArrayList<Integer>();
		sid_List.add(Final_Data.ACTIVE_SID);
		sid_List.add(Final_Data.MONITOR_SID);
		
		for (int j = 0; j <= sid_List.size()-1; j++) {

			int sid = sid_List.get(j);
			System.out.println(sid);
			
			
			logger.info("For SID  " +sid);
		if(sid==19){
			
		//	VertxBean.setCust_Mode("ACTIVE MODE TESTING");
			
			try{
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~For ACTIVE MODE SID TEST CHECK~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			logger.info("-----------------------------------First Test Case-------------------------------------");
			logger.info("For Domain Name Test Case ----> (K:_zpsbd9:<domain_name of mail ID> )");
		
			dc.email_Domain_Name_of_Mail(sid,beanhigh);

				logger.info("-----------------------------------Second Test Case-------------------------------------");
			logger.info("For Domain name with SID Test Case ----> (K:_zpsbd9:<Internal_SID>:<User name value>)");
			dc.username_Value_With_SID(sid,beanhigh);
			logger.info("-----------------------------------Third Test Case-------------------------------------");
			logger.info("For Session Test Case ---> (K:_zpsbd5:<Internal_SID>:<session value>)");
			dc.session_Value_Withoout_IP(sid,beanhigh);
			logger.info("-----------------------------------Fourth Test Case-------------------------------------");
			logger.info("For Session with IP Test Case ----> (K:_zpsbd5:<Internal_SID>:<IP>:<session value>)" );
			dc.session_Value_With_IP(sid,beanhigh);
			
			
			logger.info("-----------------------------------Fifth Test Case-------------------------------------");
			logger.info("For IP Keys IP Test Case ----> (K:_zpsbd6:<Internal_SID>:<IP>)");
			mkc.ip_Keys(sid,beanhigh);
			logger.info("-----------------------------------SIX Test Case-------------------------------------");
			logger.info("For Useragent Keys Test Case ----> (K:_zpsbd7:<Internal_SID>:<IP>:<User-agent value>)" );
			mkc.useragent_Keys(sid,beanhigh);
			logger.info("-----------------------------------Seven Test Case-------------------------------------");
			logger.info("For UZMA Keys Test Case ----> (K:__uzma:<Internal_SID>:<IP>:<__uzma value>)");
			mkc.uzma_Keys(sid,beanhigh);
			logger.info("-----------------------------------Eight Test Case-------------------------------------");
			logger.info("For UZMC Keys Test Case ----> (K:__uzmc:<Internal_SID>:<IP>:<__uzmc value after decoding>)");
			mkc.uzmc_Keys(sid,beanhigh);
			logger.info("");
			
			logger.info("For IP Keys IP Test Case ----> (K:_zpsbd6:<Internal_SID>:<IP>)");
	
			CSV_Generator cg=new CSV_Generator();
			cg.cfvread(beanhigh);
			
			}catch(Exception e){
				System.out.println(e);
			}
			
		}else if(sid==1907){
			logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~For MONITOR MODE SID TEST CHECK~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//	VertxBean.setCust_Mode("MONITOR MODE TESTING");
			try{
			logger.info("For Domain Name Test Case ----> (K:_zpsbd9:<domain_name of mail ID> )");
			dc.email_Domain_Name_of_Mail(sid,beanhigh);
			logger.info("-----------------------------------Second Test Case-------------------------------------");
			logger.info("For Domain name with SID Test Case ----> (K:_zpsbd9:<Internal_SID>:<User name value>)");
			dc.username_Value_With_SID(sid,beanhigh);
			logger.info("-----------------------------------Third Test Case-------------------------------------");
			logger.info("For Session Test Case ---> (K:_zpsbd5:<Internal_SID>:<session value>)");
			dc.session_Value_Withoout_IP(sid,beanhigh);
			logger.info("-----------------------------------Fourth Test Case-------------------------------------");
			logger.info("For Session with IP Test Case ----> (K:_zpsbd5:<Internal_SID>:<IP>:<session value>)" );
			dc.session_Value_With_IP(sid,beanhigh);
			
			
			logger.info("-----------------------------------Fifth Test Case-------------------------------------");
			logger.info("For IP Keys IP Test Case ----> (K:_zpsbd6:<Internal_SID>:<IP>)");
			mkc.ip_Keys(sid,beanhigh);
			logger.info("-----------------------------------SIX Test Case-------------------------------------");
			logger.info("For Useragent Keys Test Case ----> (K:_zpsbd7:<Internal_SID>:<IP>:<User-agent value>)" );
			mkc.useragent_Keys(sid,beanhigh);
			logger.info("-----------------------------------Seven Test Case-------------------------------------");
			logger.info("For UZMA Keys Test Case ----> (K:__uzma:<Internal_SID>:<IP>:<__uzma value>)");
			mkc.uzma_Keys(sid,beanhigh);
			logger.info("-----------------------------------Eight Test Case-------------------------------------");
			logger.info("For UZMC Keys Test Case ----> (K:__uzmc:<Internal_SID>:<IP>:<__uzmc value after decoding>)");
			mkc.uzmc_Keys(sid,beanhigh);
			}catch(Exception e){
				System.out.println(e);
			}
		}
		}
	}
	
	public static void main(String args[]) throws ParseException{
		SIDwise_Domain_Method_call sidd=new SIDwise_Domain_Method_call();
		sidd.method_call();
		
	}
}
