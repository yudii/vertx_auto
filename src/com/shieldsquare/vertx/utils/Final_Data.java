package com.shieldsquare.vertx.utils;

public final class Final_Data {

	private Final_Data() {

	}
	
	public static final int ACTIVE_SID=19;
	public static final int MONITOR_SID=1907;
	
	public static final String RULES_FAILED_1="r1";
	public static final String RULES_FAILED_2="r2";
	public static final String RULES_FAILED_4="r4";
	public static final String RULES_FAILED_5="r5";
	public static final String RULES_FAILED_6="r6";
	public static final String RULES_FAILED_10="r10";
	
	
	public static final String API_PARAMETER_0="_zpsbd0";
	public static final String API_PARAMETER_1="_zpsbd1";
	public static final String API_PARAMETER_2="_zpsbd2";
	public static final String API_PARAMETER_3="_zpsbd3";
	public static final String API_PARAMETER_4="_zpsbd4";
	public static final String API_PARAMETER_5="_zpsbd5";
	public static final String API_PARAMETER_6="_zpsbd6";
	public static final String API_PARAMETER_7="_zpsbd7";
	public static final String API_PARAMETER_8="_zpsbd8";
	public static final String API_PARAMETER_9="_zpsbd9";
	
	public static final String UZMC="__uzmc";
	public static final String UZMA="__uzma";
	
	public static final String BAD_UA_1="Java";
	public static final String GOOD_UA="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36";
	public static final String UZMA_VALUE="573b91f6d448e4.31900828";
	public static final String UZMC_VALUE="10";
	public static final String DC_IP="23.23.23.23";
	public static final String AGG_CRAW_IP="22.22.22.22";
	public static final String NORMAL_IP="22.22.22.22";
	
	public static final String DOMAIN_of_MAIL_ID="ud.com";
	public static final String HASH_VALUE="emaildomaincheck";
	public static final String ZPSBD9_VALUE="domain@ud.com";
	
	public static final String ZPSBD5_VALUE="session-value-without-ip-121344";
	public static final String ZPSBD5_VALUE2="session-123-yudi_321";
	//public static final String ZPSBD9_VALUE="domain@ud.com";
	
	public static final int EXPIRE_TIME=7200;
	
	
	
	
	
	
}
